﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AlienRace.AlienPartGenerator;

namespace SilenceTheAlien
{
	[HarmonyPatch(typeof(ExtendedGraphicTop), "Debug")]
	[HarmonyPatch(MethodType.Getter)]
	public static class SilenceTheAlien_Patch
	{
		[HarmonyPostfix]
		static bool DisableDebug(bool __result)
		{
			return false;
		}
	}
}