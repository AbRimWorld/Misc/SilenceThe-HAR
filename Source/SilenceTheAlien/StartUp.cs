﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace SilenceTheAlien
{
	[StaticConstructorOnStartup]
	public static class Startup
	{
		static Startup()
		{
			Harmony harmony = new Harmony("SilenceTheAlien");
			Harmony.DEBUG = false;//never release a build with this set to true.
			harmony.PatchAll(Assembly.GetExecutingAssembly());
		}
	}
}